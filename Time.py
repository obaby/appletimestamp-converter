import datetime
import sys
import calendar

def AppleTimeToDateTime(AppleTime):
	print(
    	datetime.datetime.fromtimestamp(
        	int(AppleTime) + int("978307200")  #2001.01.01 0:00:00 to  Jan 01 1970. (UTC) passed 978307200 seconds
    	).strftime('%Y-%m-%d %H:%M:%S')
	)
	pass
def DateTimeToAppleTime(DateTime):
        dt = datetime.datetime.strptime(DateTime, '%Y-%m-%d:%H:%M:%S')
        appleTime = int(calendar.timegm(dt.utctimetuple()))- int("978307200")
        print(appleTime)
        pass

def printusage():
        print("================================================================")
        print(sys.argv[0].split("\\")[-1] + " By obaby \n http://www.obaby.org.cn"
             + " http://www.h4ck.org.cn\n http://www.danteng.me")
        print("Usage:\n    " +sys.argv[0].split("\\")[-1])
        print("    -D Convert AppleTime to DateTime")
        print("    -A Convert Date time to AppleTime")
        print("      Time Format:2014-11-14:11:02:10")
        print("================================================================")
	pass

if __name__=="__main__":

        if len(sys.argv)>2:
                print("================================================================")
                print(sys.argv[0].split("\\")[-1] + " By obaby \n")
                if sys.argv[1] == "-D":
                        print("Apple Time :\n"+sys.argv[2])
                        print("Date Time :")
                        AppleTimeToDateTime(int(sys.argv[2]))
                        
                if sys.argv[1] == "-A":
                        print("Date Time :\n"+sys.argv[2])
                        print("Apple Time :")
                        DateTimeToAppleTime(sys.argv[2])
                        
                print("================================================================")
 	else:
                printusage()
                
